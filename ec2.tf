variable "vpc_id" {}

data "aws_vpc" "selected" {
   id = "${var.vpc_id}"
}

output "singapore-vpc" {
   value = "data.aws_vpc.selected"
}